"""Ketabkhane URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.views.generic.base import TemplateView
from Ketabkhane.apps.users.views import profile
from Ketabkhane.apps.book.views import search, book_detail, book_list

urlpatterns = [
    path('admin/', admin.site.urls),
    path("users/", include("Ketabkhane.apps.users.urls")),
    path("users/", include("django.contrib.auth.urls")),
    path('users/profile/', profile, name='profile'),
    path('book/list/', book_list, name="book_list"),
    path('book/detail/<int:id>', book_detail, name="book_detail"),
    path('book/search/', search, name="search"),
    path('', TemplateView.as_view(template_name='home.html'), name='home'),
]
