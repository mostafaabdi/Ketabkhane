from django.db import models
from django.utils.translation import gettext_lazy as _


class GenderChoices(models.TextChoices):
    MALE = _("Male")
    FEMALE = _("Female")


class UserType(models.TextChoices):
    MEMBER = _('Member')
    LIBRARIAN = _('Librarian')


class BookState(models.TextChoices):
    BORROWED = _("Borrowed")
    AVAILABLE = _("Available")
