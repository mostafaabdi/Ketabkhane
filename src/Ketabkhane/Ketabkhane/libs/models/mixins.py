from django.db import models
from django.utils.translation import gettext_lazy as _


class TimeArchiveField(models.Model):
    created = models.DateTimeField(verbose_name=_('created'), auto_now_add=True, null=True, editable=False)
    modified = models.DateTimeField(verbose_name=_('modified'), auto_now=True, null=True, editable=False)

    class Meta:
        abstract = True
