from datetime import datetime
from django.db import models


class SoftDeleteQuerySet(models.QuerySet):
    def active_items(self):
        return self.filter(deleted__isnull=True)


class SoftDeleteManager(models.Manager):
    def get_queryset(self):
        return SoftDeleteQuerySet(self.model,
                                  using=self._db).active_items()


class SoftDeleteModel(models.Model):
    deleted = models.DateTimeField(editable=False, null=True)

    objects = SoftDeleteManager()

    def delete(self, using=None, keep_parents=False):
        self.deleted = datetime.now()
        self.save()

    class Meta:
        abstract = True
