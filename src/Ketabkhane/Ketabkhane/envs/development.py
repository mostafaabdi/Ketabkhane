from .common import *

ALLOWED_HOSTS = ['*']

INSTALLED_APPS += [
    'Ketabkhane.apps.users',
    'Ketabkhane.apps.book',
    # 'crispy_forms',
]
# CRISPY_TEMPLATE_PACK = 'bootstrap4'

AUTH_USER_MODEL = "users.UserAccount"

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'Ketabkhane',
        'USER': 'odoo15',
        'PASSWORD': '12345',
        'HOST': 'localhost',
        'PORT': '5432',
    }
}

TEMPLATES[0]['DIRS'] = {'DIRS': [BASE_DIR / "templates"], }

LOGIN_REDIRECT_URL = "profile"
LOGOUT_REDIRECT_URL = "home"

