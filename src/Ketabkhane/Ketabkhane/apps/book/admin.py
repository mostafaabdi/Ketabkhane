from django.contrib import admin
from .models import *

admin.site.register(BorrowBook)
admin.site.register(Subject)
admin.site.register(Author)
# admin.site.register(Book)
admin.site.register(Publisher)


@admin.register(Book)
class BookAdmin(admin.ModelAdmin):
    list_display = ('title', 'state',)
    list_filter = ('title',)
    ordering = ('-title',)
    raw_id_fields = ('publisher', 'subject')
