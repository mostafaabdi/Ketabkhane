from django.db import models
from django.urls import reverse
from django.core.validators import ValidationError
from Ketabkhane.libs.models import mixins, choices
from django.utils.translation import gettext_lazy as _
from Ketabkhane.libs.models import soft_delete
from ..users.models import UserProfile


class Author(models.Model):
    first_name = models.CharField(_("First Name"), max_length=255, null=False, blank=False)
    last_name = models.CharField(_("Last Name"), max_length=255, null=False, blank=False)
    birth_date = models.DateField(_("Birth Date"), blank=False)

    def __str__(self):
        return self.first_name + ' ' + self.last_name


class Publisher(models.Model):
    name = models.CharField(_("Publisher Name"), max_length=255, null=False, blank=False)
    address = models.TextField(_("Address"), max_length=500, null=False, blank=True)

    def __str__(self):
        return self.name


class Subject(models.Model):
    tag = models.CharField(_("Tag"), max_length=50, null=False, blank=False)

    def save(self, *args, **kwargs):
        self.tag = self.tag + ' - '
        super(Subject, self).save(*args, **kwargs)

    def __str__(self):
        return self.tag


class Book(soft_delete.SoftDeleteModel, mixins.TimeArchiveField):
    title = models.CharField(_("Title"), max_length=255, null=False, blank=False, )
    author = models.ForeignKey(Author, on_delete=models.CASCADE)
    publisher = models.ManyToManyField(Publisher)
    page = models.IntegerField(_("Number of Pages"), default=0)
    subject = models.ManyToManyField(Subject)
    state = models.CharField(_("Book State"), max_length=12, null=False, blank=False, choices=choices.BookState.choices,
                             default=choices.BookState.AVAILABLE)

    class Meta:
        ordering = ('title',)
        constraints = [
            models.CheckConstraint(check=models.Q(page__gt=0), name='page_gt_0'),
        ]

    def get_url(self):
        return reverse('book_detail', args=[self.id])

    def __str__(self):
        return self.title


class BorrowBook(soft_delete.SoftDeleteModel, mixins.TimeArchiveField):
    book = models.ForeignKey(Book, on_delete=models.CASCADE, null=False, blank=False)
    user_profile = models.ForeignKey(UserProfile, on_delete=models.CASCADE, null=False, blank=False)
    date_from = models.DateField(_("Date From"), null=False, blank=False)
    date_to = models.DateField(_("Date To"), null=False, blank=False)

    def save(self, *args, **kwargs):
        book = Book.objects.get(id=self.book.id)
        if book.state == choices.BookState.BORROWED:
            raise ValidationError('The book is unavailable.', code='invalid')
        if self.date_to < self.date_from:
            raise ValidationError('The \'date to\' must be greater than \'date from\'', code='invalid')
        book.state = choices.BookState.BORROWED
        book.save()
        super(BorrowBook, self).save(*args, **kwargs)

    def delete(self, *args, **kwargs):
        self.book.state = choices.BookState.AVAILABLE
        super(BorrowBook, self).delete(*args, **kwargs)

    def __str__(self):
        return self.book.title + ' ' + 'borrowed to ' + self.user_profile.first_name.__str__() + ' '
        self.user_profile.last_name__str__()
