from django.shortcuts import render, get_object_or_404
from .models import Book, Subject
from django.db.models import Q
from collections import Counter


def make_result_unique(result):
    return [*Counter(result)]


def book_list(request):
    book = Book.objects.all()
    return render(request, 'book_list.html', {'book': book})


def book_detail(request, id):
    book = get_object_or_404(Book, id=id)
    types = book.subject.all()
    publishers = book.publisher.all()
    return render(request, 'book_detail.html', {'book': book, 'types': types, 'publishers': publishers})


def search(request):
    results = []
    if request.method == "GET":
        query = request.GET.get('search')
        if query == '':
            query = 'None'
        results = Book.objects.filter(
            Q(title__icontains=query) | Q(author__first_name__icontains=query)
            | Q(author__last_name__icontains=query) | Q(subject__tag__icontains=query)
        )
        results = make_result_unique(results)
    return render(request, 'book_search.html', {'query': query, 'results': results})
