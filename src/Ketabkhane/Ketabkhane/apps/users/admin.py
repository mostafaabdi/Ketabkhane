from django.contrib import admin
from .models import UserAccount, UserProfile
from django.contrib.auth.admin import UserAdmin


@admin.register(UserAccount)
class KetabkhaneUserAdmin(UserAdmin):
    ordering = ['email']
    list_display = ('email', 'is_staff', 'is_admin')
    search_fields = ['email']
    readonly_fields = ('id', 'email')

    filter_horizontal = ()
    list_filter = ()
    fieldsets = ()

@admin.register(UserProfile)
class ProfileAdmin(admin.ModelAdmin):
    list_display = ('gender', 'birth_date')
