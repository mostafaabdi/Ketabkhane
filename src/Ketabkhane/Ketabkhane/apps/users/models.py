from django.db import models
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin
from django.contrib.auth.base_user import BaseUserManager
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils.translation import gettext_lazy as _
from Ketabkhane.libs.models import mixins
from Ketabkhane.libs.models.choices import GenderChoices, UserType


class UserAccountManager(BaseUserManager):
    def create_user(self, email, password=None):
        if not email or len(email) <= 0:
            raise ValueError("Email field is required !")
        if not password:
            raise ValueError("Password is must !")

        user = self.model(
            email=self.normalize_email(email),
        )
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, password):
        user = self.create_user(
            email=self.normalize_email(email),
            password=password
        )
        user.is_admin = True
        user.is_staff = True
        user.is_superuser = True
        user.save(using=self._db)
        return user


class UserAccount(AbstractBaseUser, PermissionsMixin):
    user_type = models.CharField(max_length=12, choices=UserType.choices,
                                 default=UserType.MEMBER)
    email = models.EmailField(max_length=200, unique=True)
    is_active = models.BooleanField(default=True)
    is_admin = models.BooleanField(default=False)
    is_staff = models.BooleanField(default=False)
    is_superuser = models.BooleanField(default=False)

    is_member = models.BooleanField(default=False)
    is_librarian = models.BooleanField(default=False)

    USERNAME_FIELD = "email"

    objects = UserAccountManager()

    def __str__(self):
        return str(self.email)

    def has_perm(self, perm, obj=None):
        return self.is_admin

    def has_module_perms(self, app_label):
        return True

    def save(self, *args, **kwargs):
        if not self.user_type or self.user_type == None:
            self.user_type = UserType.MEMBER
        return super().save(*args, **kwargs)


class MemberManager(models.Manager):
    def create_user(self, email, password=None):
        if not email or len(email) <= 0:
            raise ValueError("Email field is required !")
        if not password:
            raise ValueError("Password is must !")
        email = email.lower()
        user = self.model(
            email=email
        )
        user.set_password(password)
        user.save(using=self._db)
        return user

    def get_queryset(self, *args, **kwargs):
        queryset = super().get_queryset(*args, **kwargs)
        queryset = queryset.filter(type=UserType.MEMBER)
        return queryset


class Member(UserAccount):
    class Meta:
        proxy = True

    objects = MemberManager()

    def save(self, *args, **kwargs):
        self.type = UserType.MEMBER
        self.is_member = True
        return super().save(*args, **kwargs)


class LibrarianManager(models.Manager):
    def create_user(self, email, password=None):
        if not email or len(email) <= 0:
            raise ValueError("Email field is required !")
        if not password:
            raise ValueError("Password is must !")
        email = email.lower()
        user = self.model(
            email=email
        )
        user.set_password(password)
        user.save(using=self._db)
        return user

    def get_queryset(self, *args, **kwargs):
        queryset = super().get_queryset(*args, **kwargs)
        queryset = queryset.filter(type=UserType.LIBRARIAN)
        return queryset


class Librarian(UserAccount):
    class Meta:
        proxy = True

    objects = LibrarianManager()

    def save(self, *args, **kwargs):
        self.type = UserType.LIBRARIAN
        self.is_librarian = True
        return super().save(*args, **kwargs)


class UserProfile(mixins.TimeArchiveField):
    user = models.OneToOneField(UserAccount, on_delete=models.CASCADE)
    first_name = models.CharField(_("First Name"), max_length=255, null=True, blank=True, )
    last_name = models.CharField(_("Last Name"), max_length=255, null=True, blank=True, )
    gender = models.CharField(_("Gender"), max_length=12, blank=True, choices=GenderChoices.choices)
    birth_date = models.DateField(_('Birth Date'), null=True, blank=True)

    def __str__(self):
        return str(self.user)

    @receiver(post_save, sender=UserAccount)
    def create_user_profile(sender, instance, created, **kwargs):
        if created:
            UserProfile.objects.create(user=instance)
