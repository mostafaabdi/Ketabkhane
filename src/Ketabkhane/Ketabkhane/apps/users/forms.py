from django import forms
from .models import UserAccount, UserProfile


class UserRegisterForm(forms.ModelForm):
    class Meta:
        model = UserAccount
        fields = ['email', 'password', 'user_type']

    def save(self, commit=True):
        user = super(UserRegisterForm, self).save(commit=False)
        user.set_password(self.cleaned_data["password"])
        if commit:
            user.save()
        return user


class UserUpdateForm(forms.ModelForm):
    email = forms.EmailField()

    class Meta:
        model = UserAccount
        fields = ['email']


class ProfileUpdateForm(forms.ModelForm):
    class Meta:
        model = UserProfile
        fields = ['first_name', 'last_name', 'gender', 'birth_date']
